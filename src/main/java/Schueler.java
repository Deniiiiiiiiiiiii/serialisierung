import java.io.*;
import java.util.ArrayList;

/**
 * Created: 19.03.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public class Schueler {

    private String vorname, nachname;
    private transient int kennung;
    private transient long sozialversicherung;

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public int getKennung() {
        return kennung;
    }

    public long getSozialversicherung() {
        return sozialversicherung;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public void setKennung(int kennung) {
        if (isValidKennung(kennung)) {
            this.kennung = kennung;
        } else {
            throw new IllegalArgumentException("Ungültige Kennung!");
        }
    }

    private boolean isValidKennung(Integer kennung) {
        if (kennung.toString().length() != 5){
            return false;
        }else {
            return true;
        }
    }

    public void setSozialversicherung(long sozialversicherung) {
        this.sozialversicherung = sozialversicherung;
    }

    public Schueler(String vorname, String nachname, int kennung, long sozialversicherung) {
        this.vorname = vorname;
        this.nachname = nachname;
        if (isValidKennung(kennung)) {
            this.kennung = kennung;
        } else {
            throw new IllegalArgumentException("Ungültige Kennung!");
        }
        this.sozialversicherung = sozialversicherung;
    }

    public static void main(String[] args) {
        ArrayList<Schueler> schuelers = new ArrayList<>();

        schuelers.add(new Schueler("Denisa", "Barbos", 14758, 1111111111));
        schuelers.add(new Schueler("Ioana", "Barbos", 21345, 222222222));
        schuelers.add(new Schueler("idk", "Max", 78212, 1809027405));
        schuelers.add(new Schueler("Name", "Mustermann", 53412, 2108066580));
        schuelers.add(new Schueler("Random", "Barbos", 92312, 2108066500));

        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {

            for(Schueler s: schuelers){
                s.writeObject(objectOutputStream);
            }


            while (byteArrayInputStream.available() > 0){
                for (Schueler schueler: schuelers){
                    schueler.readObject(objectInputStream);
                    Schueler y = (Schueler) objectInputStream.readObject();
                    System.out.println(y);
                }

            }

        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }

        for(Schueler schueler: schuelers){
            System.out.println(schueler);
        }
    }

    private static String encode(Long number){
        String nr = String.valueOf(number);
        StringBuilder encoded = new StringBuilder();

        for (int i = 0; i < nr.length(); i++) {
            char digit = (char) (nr.charAt(i) + 3);

            encoded.append(digit);
        }

        return encoded.toString();
    }

    private static Long decode(String cipher){
        StringBuilder decoded = new StringBuilder();

        for (int i = 0; i < cipher.length(); i++) {
            char decodedChar = (char) (cipher.charAt(i) - 3);

            decoded.append(decodedChar);
        }

        return Long.parseLong(decoded.toString());
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeUTF(encode((long) kennung));
        out.writeUTF(encode(sozialversicherung));
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        kennung = Integer.parseInt(decode(in.readUTF()).toString());
        sozialversicherung = Long.parseLong(decode(in.readUTF()).toString());

    }

    @Override
    public String toString() {
        return "Schueler{" +
                "vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                ", kennung=" + kennung +
                ", sozialversicherung=" + sozialversicherung +
                '}';
    }
}
